package org.slingerxv.limitart.funcs;

public interface Func<R> {
	R run();
}
