package org.slingerxv.limitart.funcs;

public interface Func3<T1, T2, T3, R> {
	R run(T1 t1, T2 t2, T3 t3);
}
