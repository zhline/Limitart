package org.slingerxv.limitart.funcs;

public interface Func1<T, R> {
	R run(T t);
}
