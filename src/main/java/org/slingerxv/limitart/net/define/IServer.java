package org.slingerxv.limitart.net.define;

public interface IServer {
	void startServer() throws Exception;

	void stopServer() throws Exception;
}
