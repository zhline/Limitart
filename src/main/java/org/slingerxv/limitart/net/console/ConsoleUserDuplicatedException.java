package org.slingerxv.limitart.net.console;

public class ConsoleUserDuplicatedException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConsoleUserDuplicatedException(String info) {
		super(info);
	}
}
