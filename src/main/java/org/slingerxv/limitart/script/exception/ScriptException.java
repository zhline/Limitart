package org.slingerxv.limitart.script.exception;

public class ScriptException extends Exception {
	private static final long serialVersionUID = 1L;

	public ScriptException(String info) {
		super(info);
	}
}
